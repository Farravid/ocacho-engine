/************************************************************************/
/* Base class for all the entities/classes of Ocacho engine.
*
* The classes that are derived from BaseObject should implement the macro
* GENERATE_CLASS_METADATA(T class)
* to create the basic structure of an Ocacho object.
*
* This allow us to cast to different objects and check
* if an object is another one easily.
/************************************************************************/

#pragma once

#include "ogpch.h"

namespace Ocacho
{
	class BaseObject
	{
	public:

		static std::string getClassNameStatic()
		{
			static std::string className = "ObjectBase";
			return className;
		}

		inline virtual std::string getClassName() const { return BaseObject::getClassNameStatic(); }

		template<class T>
		bool isA() const
		{
			return dynamic_cast<T*>(this) != nullptr;
		}

		template<class T>
		T* castTo()
		{
			T* res = dynamic_cast<T*>(this);

			OG_ENGINE_ASSERT(res, "Cast to failed. Trying to from {0} to cast to {1}", getClassName() ,res->getClassName());

			return res;
		}
	};
}

