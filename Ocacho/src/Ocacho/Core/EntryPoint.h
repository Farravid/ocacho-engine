/************************************************************************/
/* Entry point of the Ocacho engine.
* 
* Main function
/************************************************************************/

#pragma once

extern Ocacho::Application* Ocacho::createApplication();

int main(int argc, char** argv)
{
	Ocacho::Log::init();
	OG_ENGINE_LOG_WARN("Initialized Log!");
	
	Ocacho::Input::getInstance();
	OG_ENGINE_LOG_WARN("Initialized Input");

	auto app = Ocacho::createApplication();
	app->run();
	delete app;
}