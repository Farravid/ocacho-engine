/************************************************************************/
/* Singleton template for creating singletons easily.
*
* The classes that are singleton should derived from Singleton providing
* the class between <> like : public Singleton<Application>
* 
* Singleton is derived from BaseObject, so all classes derived from Singleton
* should provide the macro: GENERATE_CLASS_METADATA to provide
* BaseObject methods and implementations (isA, castTo, getName)...
/************************************************************************/

#pragma once

#include "BaseObject.h"

namespace Ocacho
{
	template<class T>
	class Singleton  : public BaseObject
	{
	protected:
		static T* m_instance;
	public:
		static T* getInstance()
		{
			if (m_instance == nullptr)
			{
				OG_ENGINE_LOG_INFO("Creating a Singleton of class {0}", T::getClassNameStatic());
				m_instance = new T;
			}

			return m_instance;
		};
	};

	template<class T>
	T* Singleton<T>::m_instance = nullptr;
}