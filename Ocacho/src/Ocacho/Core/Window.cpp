#include "ogpch.h"
#include "Window.h"
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "Ocacho/Events/ApplicationEvent.h"
#include "Ocacho/Events/KeyEvent.h"
#include "Ocacho/Events/MouseEvent.h"


namespace Ocacho
{
	static bool s_GLFWInitialized = false;

	static void glfwErrorCallback(int error, const char* description)
	{
		OG_ENGINE_LOG_ERROR("glfw Error {0}, description: {1}", error, description);
	}

	Window::Window(const WindowProps& props /*= WindowProps()*/)
	{
		m_props.title = props.title;
		m_props.width = props.width;
		m_props.height = props.height;
		m_props.vSync = props.vSync;

		OG_ENGINE_LOG_INFO("Creating window {0} ({1}, {2})", props.title, props.width, props.height);
		
		init();
	}

	Window::~Window()
	{
		
	}

	void Window::onUpdate()
	{
		glfwPollEvents();
		glfwSwapBuffers(m_window);
	}

	void Window::setVSync(bool enabled)
	{
		if (enabled)
			glfwSwapInterval(1);
		else
			glfwSwapInterval(0);

		m_props.vSync = enabled;
	}

	void Window::init()
	{
		/*Init GLFW*/
		if (!s_GLFWInitialized)
		{
			int success = glfwInit();
			OG_ENGINE_ASSERT(success, "Could not intialize GLFW!");
			glfwSetErrorCallback(glfwErrorCallback);

			s_GLFWInitialized = true;
		}

		//Create the window
		m_window = glfwCreateWindow((int)m_props.width, (int)m_props.height, m_props.title.c_str(), nullptr, nullptr);
		//Create the glfw context
		glfwMakeContextCurrent(m_window);

		/*Init GLAD*/
		int status = gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
		OG_ENGINE_ASSERT(status, "Could not intialize GLAD!");

		//Set the window user pointer to this to be able to access to the window variables in the glfw callbacks
		glfwSetWindowUserPointer(m_window, this);

		setVSync(true);

		initCallbacks();
	}

	void Window::initCallbacks()
	{
		glfwSetWindowSizeCallback(m_window, [](GLFWwindow* window, int width, int height)
			{
				Window* ww = (Window*)(glfwGetWindowUserPointer(window));

				WindowResizeEvent event(width, height);
				ww->callOnEventOnListeners(event);

				ww->m_props.width = width;
				ww->m_props.height = height;
			});

		glfwSetWindowCloseCallback(m_window, [](GLFWwindow* window)
			{
				Window* ww = (Window*)(glfwGetWindowUserPointer(window));

				WindowCloseEvent event;
				ww->callOnEventOnListeners(event);
			});

		glfwSetKeyCallback(m_window, [](GLFWwindow* window, int key, int scancode, int action, int mods)
			{
				Window* ww = (Window*)(glfwGetWindowUserPointer(window));

				switch (action)
				{
				case GLFW_PRESS:
				{
					KeyPressedEvent event(key, 0);
					ww->callOnEventOnListeners(event);
					break;
				}
				case GLFW_RELEASE:
				{
					KeyReleasedEvent event(key);
					ww->callOnEventOnListeners(event);
					break;
				}
				case GLFW_REPEAT:
				{
					KeyPressedEvent event(key, 1);
					ww->callOnEventOnListeners(event);
					break;
				}


				}
			});

		glfwSetCharCallback(m_window, [](GLFWwindow* window, unsigned int key)
			{
				Window* ww = (Window*)(glfwGetWindowUserPointer(window));

				KeyTypedEvent event(key);
				ww->callOnEventOnListeners(event);

			});

		glfwSetMouseButtonCallback(m_window, [](GLFWwindow* window, int button, int action, int mods)
			{
				Window* ww = (Window*)(glfwGetWindowUserPointer(window));

				switch (action)
				{
				case GLFW_PRESS:
				{
					MouseButtonPressedEvent event(button);
					ww->callOnEventOnListeners(event);
					break;
				}
				case GLFW_RELEASE:
				{
					MouseButtonReleasedEvent event(button);
					ww->callOnEventOnListeners(event);
					break;
				}
				}
			});

		glfwSetScrollCallback(m_window, [](GLFWwindow* window, double offsetX, double offsetY)
			{
				Window* ww = (Window*)(glfwGetWindowUserPointer(window));

				MouseScrolledEvent event((float)offsetX, (float)offsetY);
				ww->callOnEventOnListeners(event);

			});

		glfwSetCursorPosCallback(m_window, [](GLFWwindow* window, double posX, double posY)
			{
				Window* ww = (Window*)(glfwGetWindowUserPointer(window));

				MouseMovedEvent event((float)posX, (float)posY);
				ww->callOnEventOnListeners(event);

			});
	}

	void Window::shutdown()
	{
		glfwDestroyWindow(m_window);
	}

	bool Window::isEventListenerContained(EventListener* e)
	{
		return std::find(m_eventListeners.begin(), m_eventListeners.end(), e) != m_eventListeners.end();
	}

	void Window::callOnEventOnListeners(Event& e)
	{
		for (EventListener* listener : m_eventListeners)
			listener->onEvent(e);
	}

	bool Window::registerEventListener(EventListener* e)
	{
		if (!isEventListenerContained(e))
		{
			m_eventListeners.push_back(e);
			return true;
		}

		return false;
	}

	bool Window::unregisterEventListener(EventListener* e)
	{
		if (isEventListenerContained(e))
		{
			for (int i = 0; i < m_eventListeners.size(); i++)
			{
				if (m_eventListeners[i] == e)
				{
					m_eventListeners.erase(m_eventListeners.begin() + i);
					return true;
				}
			}

			return false;
		}

		return false;
	}
}