/************************************************************************/
/* Derived Application events from event
*
* Events for managing all the application and window events.
/************************************************************************/

#pragma once

#include "Event.h"

namespace Ocacho {

	/************************************************************************/
	/* Window Resize Event
	/************************************************************************/
	class WindowResizeEvent : public Event
	{
		PRIVATE(unsigned int, GET_INLINE, m_width, Width);
		PRIVATE(unsigned int, GET_INLINE, m_height, Height);

	public:
		WindowResizeEvent(unsigned int width, unsigned int height)
			: m_width(width), m_height(height) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "WindowResizeEvent: " << m_width << ", " << m_height;
			return ss.str();
		}

		EVENT_CLASS_TYPE(WindowResize)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};

	/************************************************************************/
	/* Window Close Event.
	/************************************************************************/
	class WindowCloseEvent : public Event
	{
	public:
		WindowCloseEvent() {}

		EVENT_CLASS_TYPE(WindowClose)
		EVENT_CLASS_CATEGORY(EventCategoryApplication)
	};
}