/************************************************************************/
/* Base event for Ocacho event system.
*
* Events in Ocacho are currently blocking, meaning when an event occurs it
* immediately gets dispatched and must be dealt with right then an there.
* For the future, a better strategy might be to buffer events in an event
* bus and process them during the "event" part of the update stage.
/************************************************************************/

#pragma once

#include "ogpch.h"

namespace Ocacho {

	/************************************************************************/
	/* Enums for type and category of the events.
	/************************************************************************/

	enum class EventType
	{
		None = 0,
		WindowClose, WindowResize, WindowFocus, WindowLostFocus, WindowMoved,
		KeyPressed, KeyReleased, KeyTyped,
		MouseButtonPressed, MouseButtonReleased, MouseMoved, MouseScrolled
	};

	enum EventCategory
	{
		None = 0,
		EventCategoryApplication    = BIT(0),
		EventCategoryInput          = BIT(1),
		EventCategoryKeyboard       = BIT(2),
		EventCategoryMouse          = BIT(3),
		EventCategoryMouseButton    = BIT(4)
	};

	/************************************************************************/
	/* Macros for events definition.
	* 
	* These should be called in all the derived class of Event.
	* To set the type and category of each event.
	/************************************************************************/

	#define EVENT_CLASS_TYPE(type) static EventType getStaticType() { return EventType::type; }\
										virtual EventType getEventType() const override { return getStaticType(); }\
										virtual const char* getName() const override { return #type; }

	#define EVENT_CLASS_CATEGORY(category) virtual int getCategoryFlags() const override { return category; }

	/************************************************************************/
	/* Base event class.
	/************************************************************************/
	class Event
	{		
	public:
		/**
		* Wheter an event is handled or not
		*/
		bool handled = false;

	public:
		/**
		* Return the type of this event as a EventType enum.
		*/
		virtual EventType getEventType() const = 0;

		/**
		* Return the name of the type of this event.
		*/
		virtual const char* getName() const = 0;

		/**
		* Return the category of this event as a int of EventCategory enum.
		*/
		virtual int getCategoryFlags() const = 0;

		/**
		* Event to string
		*/
		virtual std::string toString() const { return getName(); }

		/**
		* Wheter an event is within a category or not
		*/
		inline bool isInCategory(EventCategory category) { return getCategoryFlags() & category; }
	};

	/************************************************************************/
	/* EventDispatcher
	* 
	* This auxiliary class allow us to dispatch coming events.
	* As an example check -> Application::onEvent()
	/************************************************************************/
	class EventDispatcher
	{
		template<typename T>
		using EventFn = std::function<bool(T&)>;

	private:
		Event& m_event;

	public:
		EventDispatcher(Event& event): m_event(event) {}

		/**
		* Dispath to the parameter function in case our event is equals to the T event.
		* 
		* @param func The function that is gonna be dispatched after the event occurs
		* 
		* @param example1: dispatcher.dispatch<WindowCloseEvent>(OG_BIND_EVENT_FN(Application::onWindowClosed));
		* @param example2: dispatcher.dispatch<WindowCloseEvent>(std::bind(&Application::onWindowClosed, this, std::placeholders::_1));
		*/
		template<typename T>
		bool dispatch(EventFn<T> func)
		{
			if (m_event.getEventType() == T::getStaticType())
			{
				m_event.handled = func(*(T*)&m_event);
				return true;
			}
			return false;
		}
	};

	/* Allowing to cout events */
	inline std::ostream& operator<<(std::ostream& os, const Event& e) { return os << e.toString(); }
}

