/************************************************************************/
/* Derived Key events from event
*
* Events for managing all the key input events.
/************************************************************************/

#pragma once

#include "Event.h"

namespace Ocacho {

	/************************************************************************/
	/* Base Key event
	* 
	* This is gonna be the base Key event for the derived Key events.
	* This contains the keycode.
	/************************************************************************/
	class KeyEvent : public Event
	{
		PROTECTED(int, GET_INLINE, m_keyCode, KeyCode);

	protected:
		KeyEvent(int keycode): m_keyCode(keycode) {}

	public:
		EVENT_CLASS_CATEGORY(EventCategoryKeyboard | EventCategoryInput)
	};

	/************************************************************************/
	/* Key Pressed Event
	/************************************************************************/
	class KeyPressedEvent : public KeyEvent
	{
		PRIVATE(int, GET_INLINE, m_repeatCount, RepeatCount);

	public:
		KeyPressedEvent(int keycode, int repeatCount) : KeyEvent(keycode), m_repeatCount(repeatCount) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "KeyPressedEvent: " << m_keyCode << " (" << m_repeatCount << " repeats)";
			return ss.str();
		}

		EVENT_CLASS_TYPE(KeyPressed)
	};

	/************************************************************************/
	/* Key Released Event
	/************************************************************************/
	class KeyReleasedEvent : public KeyEvent
	{
	public:
		KeyReleasedEvent(int keycode) : KeyEvent(keycode) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "KeyReleasedEvent: " << m_keyCode;
			return ss.str();
		}

		EVENT_CLASS_TYPE(KeyReleased)
	};


	/************************************************************************/
	/* Key Typed Event
	/************************************************************************/
	class KeyTypedEvent : public KeyEvent
	{
	public:
		KeyTypedEvent(int keycode) : KeyEvent(keycode) {}

		std::string toString() const override
		{
			std::stringstream ss;
			ss << "KeyTypedEvent: " << m_keyCode;
			return ss.str();
		}

		EVENT_CLASS_TYPE(KeyTyped)
	};
}