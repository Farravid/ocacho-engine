/************************************************************************/
/* ImGui layer.
*
* No additional methods, just overrides and init and shutdown
/************************************************************************/

#pragma once

#include "Ocacho/Core/Layer.h"

namespace Ocacho
{
	class  ImGuiLayer : public Layer
	{
	public:
		ImGuiLayer();
		~ImGuiLayer();

		virtual void onAttach() override;
		virtual void onDetach() override;
		virtual void onRender() override;

		void init();
		void shutdown();
	};
}


