/************************************************************************/
/* Input system for Ocacho engine.
*
* This class provides the methods for handling the input system.
* 
* In case we need to override methods per platform, we can override them
* in a separate platform input class.
/************************************************************************/

//TODO: Probably we need to implement more input functions like keyUp, keyDow, only press one time...

#pragma once

#include "ogpch.h"
#include "Ocacho/Core/Singleton.h"

namespace Ocacho
{
	class Input : public Singleton<Input>
	{
	protected:

		/**
		* Wheter the keycode is pressed or not. (Implementation)
		* 
		* @param keycode: Keycode pressed.
		*/
		virtual bool isKeyPressedImpl(int keycode);

		/**
		* Wheter the mouse button is pressed or not. (Implementation)
		*
		* @param button: Mouse button pressed.
		*/
		virtual bool isMouseButtonPressedImpl(int button);

		/**
		* Current position of the mouse in the whole screen not only the window (Implementation)
		*/
		virtual std::pair<float, float> getMousePositionImpl();

		/**
		* Current X position of the mouse in the whole screen not only the window (Implementation)
		*/
		virtual float getMouseXImpl();

		/**
		* Current Y position of the mouse in the whole screen not only the window (Implementation)
		*/
		virtual float getMouseYImpl();

	public:
		GENERATE_CLASS_METADATA(Input)

		/*Return the isKeyPressedImpl of the current platform*/
		inline static bool isKeyPressed(int keycode) { return m_instance->isKeyPressedImpl(keycode); }

		/*Return the isMouseButtonPressedImpl of the current platform*/
		inline static bool isMouseButtonPressed(int button) { return m_instance->isMouseButtonPressedImpl(button); }

		/*Return the getMousePositionImpl of the current platform*/
		inline static std::pair<float, float> getMousePosition() { return m_instance->getMousePositionImpl(); }

		/*Return the getMouseXImpl of the current platform*/
		inline static float getMouseX() { return m_instance->getMouseXImpl(); }

		/*Return the getMouseYImpl of the current platform*/
		inline static float getMouseY() { return m_instance->getMouseYImpl(); }
	};
}