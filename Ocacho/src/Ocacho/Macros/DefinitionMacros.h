/************************************************************************/
/* This are the definition macros for Ocacho engine.
*
* This allow us to define a variable and create its get or set or both methods in one line.
/************************************************************************/

#pragma once

/*-----------------------------------------------------------------------------
Definition macros
-----------------------------------------------------------------------------*/

#define PRIVATE(type, accessor, name, faceName)\
    private:\
        type name = {};\
    public:\
		accessor(name,type, faceName)\

#define PRIVATE_STATIC(type, accessor, name, faceName)\
    private:\
        static type name;\
    public:\
		accessor(name,type, faceName)\

#define PROTECTED(type, accessor, name, faceName)\
    protected:\
        type name = {};\
    public:\
		accessor(name,type, faceName)\

#define PROTECTED_STATIC(type, accessor, name, faceName)\
    protected:\
        static type name;\
    public:\
		accessor(name,type, faceName)\

/*-----------------------------------------------------------------------------
Accessors
-----------------------------------------------------------------------------*/

#define NONE(name,type,faceName)

#define GET(name, type, faceName)\
	const type get##faceName() const{ \
            return name;\
	};\

#define GET_INLINE(name, type, faceName)\
	inline type get##faceName() const{ \
            return name;\
	};\

#define GET_STATIC(name, type, faceName)\
	static type get##faceName() { \
            return name;\
	};\

#define GET_INLINE_STATIC(name, type, faceName)\
	inline static type get##faceName() { \
            return name;\
	};\

#define SET(name,type, faceName)\
	void set##faceName(type value){ \
	name = value;\
	};\

#define SET_INLINE(name,type, faceName)\
	inline void set##faceName(type value){ \
	name = value;\
	};\

#define GET_SET(name, type, faceName)\
	const type get##faceName() const{ \
		return name;\
	};\
	void set##faceName(type value){ \
		name = value;\
	};\

#define GET_SET_INLINE(name, type, faceName)\
	inline type get##faceName() const{ \
		return name;\
	};\
	inline void set##faceName(type value){ \
		name = value;\
	};\

