/************************************************************************/
/* Profiling system for Ocacho engine.
* 
* This system is only for debug purposes.
* They will be disabled in the Release/Shipping version
/************************************************************************/

#pragma once

/*In order to ouput the profiling, would you like to see the output on the console or on the IMGUI text*/
#define IMGUI_OUTPUT    0
#define CONSOLE_OUTPUT  1

#include <chrono>
#include "Ocacho/Log/Log.h"

using timePoint = std::chrono::time_point<std::chrono::high_resolution_clock>;

namespace Ocacho
{
	class ProfilingTimer
	{   
		private:
			const char* m_name;
			uint8_t     m_outputType;
			timePoint   m_startTimePoint;
			u_int8_t    m_stopped;

		public:
			ProfilingTimer(const char* p_name, uint8_t p_outputType);
			~ProfilingTimer();

			void stopTimer();
	};
}

//TODO: This should be change by a make - premake definition 
#define OG_ENABLE_PROFILING 1

#if OG_ENABLE_PROFILING
	/*Note that this will stop the counter when exit one scope, be careful not use them with if, for...
	FOR NOW: This will provoke an error in case you are debugging a function with the same name*/
	#define OG_PROFILE_FUNCTION(output)  OG_PROFILE_SCOPE(__FUNCTION__,output)
	#define OG_PROFILE_SCOPE(name,output) Ocacho::ProfilingTimer timer##__LINE__(name,output);
#else
	#define OG_PROFILE_FUNCTION(output)  
	#define OG_PROFILE_SCOPE(name,output)
#endif