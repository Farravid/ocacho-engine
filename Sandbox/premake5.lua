project "Sandbox"
	kind "ConsoleApp"
	language "C++"
	cppdialect "C++17"
	staticruntime "on"

    targetdir ("%{wks.location}/bin/" .. outputdir .. "/%{prj.name}")
	objdir ("%{wks.location}/bin-int/" .. outputdir .. "/%{prj.name}")

	files
	{
		"src/**.h",
		"src/**.cpp"
	}

	includedirs
	{
		"%{wks.location}/Ocacho/vendor/spdlog/include",
		"%{wks.location}/Ocacho/src",
		"%{wks.location}/Ocacho/vendor",
		"%{IncludeDir.glm}"
	}

	links
	{
		"Ocacho"
	}

	defines
	{
		"OG_ENABLE_ASSERTS"
	}


    filter "system:linux"
        systemversion "latest"
		pic "off"

        defines
        {
            "OG_PLATFORM_LINUX",
        }

		links
		{
			"GLFW",
			"Glad",
			"ImGui",
			"GLEW",
			"Xrandr",
			"Xi",
			"GLU",
			"GL",
			"X11",
			"dl",
			"pthread",
			"stdc++fs"
		}

    filter "system:windows"
        systemversion "latest"

        defines
        {
            "OG_PLATFORM_WINDOWS"
        }

    filter "configurations:Debug"
        defines "OG_DEBUG"
        runtime "Debug"
        symbols "on"

    filter "configurations:Release"
        defines "OG_RELEASE"
        runtime "Release"
        optimize "on"

    filter "configurations:Shipping"
        defines "OG_SHIPPING"
        runtime "Release"
        optimize "on"