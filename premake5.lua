include "Dependencies.lua"

workspace "Ocacho"
	architecture "x64"
	startproject "Sandbox"

	configurations
	{
		"Debug",
		"Release",
		"Shipping"
	}

	flags
	{
		"MultiProcessorCompile"
	}

outputdir = "%{cfg.buildcfg}-%{cfg.system}-%{cfg.architecture}"

group "Dependencies"
	include "Ocacho/vendor/GLFW"
	include "Ocacho/vendor/Glad"
	include "Ocacho/vendor/imgui"
group ""

include "Ocacho"
include "Sandbox"